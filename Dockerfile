FROM node:14-alpine AS build

WORKDIR /app
COPY . .

RUN yarn install && yarn build

FROM nginx:1.15 as final
COPY --from=build /app/build/ /usr/share/nginx/html
# Copy the default nginx.conf provided by tiangolo/node-frontend
COPY --from=build /app/nginx.conf /etc/nginx/conf.d/default.conf
