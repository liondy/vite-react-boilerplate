FROM node:14-alpine
WORKDIR /app

COPY . .
RUN yarn install

RUN yarn run build-storybook

EXPOSE 6006
CMD ["yarn","run","storybook"]
