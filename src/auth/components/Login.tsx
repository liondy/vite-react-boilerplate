import { Box, Typography, Button, CircularProgress } from '@mui/material'

interface LoginProps {
  isDisabled?: boolean
  isLoading?: boolean
}

const LoginComponent = ({ isDisabled = false, isLoading = false }: LoginProps) => (
  <Box sx={{ textAlign: 'center' }}>
    <Typography variant="h3">React Vite Boilerplate</Typography>
    <Button type="submit" disabled={isDisabled} variant="contained" sx={{ mt: '1rem' }}>
      {isLoading ? <CircularProgress /> : 'Login'}
    </Button>
  </Box>
)

export default LoginComponent
