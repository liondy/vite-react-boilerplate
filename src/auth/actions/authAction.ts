import { Dispatch } from '@reduxjs/toolkit'
import AuthService from '../../api/AuthService'
import { API } from '../../constants'
import { setToken } from '../../utils/storage'
import { FAILED_LOGIN, LOADING_LOGIN, SUCCESS_LOGIN } from '../interfaces/Login'
import { loadingLogin, loginError, loginSuccess, clearState } from '../slices/authSlice'

export const loginAction = (dataToPost: any) => async (dispatch: Dispatch) => {
  const service = new AuthService()
  dispatch(loadingLogin({ type: LOADING_LOGIN, isLoading: true }))
  const { data, statusCode, message } = await service.create({ ...dataToPost }, API.AUTH.LOGIN)
  if (statusCode === 404 || statusCode === 405) {
    setToken('app__token')
    const dummyUser = {
      id: 'EM0001',
      email: 'administrator@gmail.com',
      token: 'app__token',
      refreshToken: 'app__refresh_token',
    }
    dispatch(loginSuccess({ type: SUCCESS_LOGIN, payload: dummyUser, messageSuccess: 'Success Login' }))
  } else {
    if (statusCode === 200) {
      dispatch(loginSuccess({ type: SUCCESS_LOGIN, payload: data, messageSuccess: 'Success login' }))
    } else {
      dispatch(
        loginError({
          type: FAILED_LOGIN,
          messageError: message || '',
        })
      )
    }
  }
  dispatch(loadingLogin({ type: LOADING_LOGIN, isLoading: false }))
}

export const removeMessage = () => async (dispatch: Dispatch) => {
  dispatch(clearState())
}
