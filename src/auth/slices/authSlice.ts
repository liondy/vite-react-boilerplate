import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { IAuthenticatedUser, IFailedLogin, ILoadingLogin, ISuccessLogin } from '../interfaces/Login'

interface IAuthInitialState {
  token: string
  refreshToken: string
  authenticatedUser: IAuthenticatedUser
  isLoadingLogin: boolean
  messageSuccessLogin: string
  messageErrorLogin: string
}

const authInitialState: IAuthInitialState = {
  token: '',
  refreshToken: '',
  authenticatedUser: {} as IAuthenticatedUser,
  isLoadingLogin: false,
  messageSuccessLogin: '',
  messageErrorLogin: '',
}

const auth = createSlice({
  name: 'auth',
  initialState: authInitialState,
  reducers: {
    loadingLogin: (state, { payload }: PayloadAction<ILoadingLogin>) => ({
      ...state,
      isLoadingLogin: payload.isLoading,
    }),
    loginSuccess: (state, { payload }: PayloadAction<ISuccessLogin>) => ({
      ...state,
      messageSuccessLogin: payload.messageSuccess,
      authenticatedUser: payload.payload,
      token: payload.payload.token,
      refreshToken: payload.payload.refreshToken,
    }),
    loginError: (state, { payload }: PayloadAction<IFailedLogin>) => ({
      ...state,
      messageErrorLogin: payload.messageError,
    }),
    clearState: (state) => {
      return {
        ...state,
        messageErrorLogin: '',
        messageSuccessLogin: '',
      }
    },
  },
})

export const authReducer = auth.reducer
export const { loadingLogin, loginSuccess, loginError, clearState } = auth.actions
