export const LOADING_LOGIN = 'LOADING_LOGIN'
export const SUCCESS_LOGIN = 'SUCCESS_LOGIN'
export const FAILED_LOGIN = 'FAILED_LOGIN'

export interface IAuthenticatedUser {
  id: string
  email: string
  token: string
  refreshToken: string
}

export interface ILoadingLogin {
  type: typeof LOADING_LOGIN
  isLoading: boolean
}

export interface ISuccessLogin {
  type: typeof SUCCESS_LOGIN
  messageSuccess: string
  payload: IAuthenticatedUser
}

export interface IFailedLogin {
  type: typeof FAILED_LOGIN
  messageError: string
}
