import { useEffect } from 'react'
import { Box } from '@mui/material'
import { useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import { useAppDispatch, useAppSelector } from '../../store'
import LoginComponent from '../components/Login'
import { loginAction } from '../'
import { Toast } from '../../app'
import { removeMessage } from '../actions/authAction'

export const Login = () => {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const { isLoadingLogin, token, messageErrorLogin, messageSuccessLogin } = useAppSelector((state) => state.auth)

  const { handleSubmit } = useForm({
    reValidateMode: 'onSubmit',
  })

  const onSubmit = () => {
    dispatch(loginAction({ username: '', password: '' }))
  }

  const handleClose = () => {
    dispatch(removeMessage())
  }

  useEffect(() => {
    let loginTimeout: any = null
    if (token !== '') {
      loginTimeout = setTimeout(() => {
        navigate('/')
      }, 1000)
    }
    return () => {
      clearTimeout(loginTimeout)
    }
  }, [token, navigate])

  return (
    <Box
      sx={{
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
        padding: '1.5rem',
        height: '100vh',
      }}
    >
      <form onSubmit={handleSubmit(onSubmit)}>
        <LoginComponent isLoading={isLoadingLogin} isDisabled={isLoadingLogin} />
      </form>
      <Toast open={messageSuccessLogin !== ''} message={messageSuccessLogin} handleClose={handleClose} />
      <Toast severity="error" open={messageErrorLogin !== ''} message={messageErrorLogin} handleClose={handleClose} />
    </Box>
  )
}
