import jwt from 'jwt-decode'
import { CONSTANTS } from '../constants'

export function setToken(value: any) {
  localStorage.setItem(CONSTANTS.TOKEN_STORAGE, value)
}

export function getToken() {
  return localStorage.getItem(CONSTANTS.TOKEN_STORAGE) || ''
}

export function setRefreshToken(value: string) {
  localStorage.setItem(CONSTANTS.REFRESH_TOKEN, value)
}

export function getRefreshToken() {
  return localStorage.getItem(CONSTANTS.REFRESH_TOKEN) || ''
}

export function setExpiredToken(value: any) {
  localStorage.setItem(CONSTANTS.EXPIRE_TOKEN_TIME, value)
}

export function getExpiredToken() {
  return localStorage.getItem(CONSTANTS.EXPIRE_TOKEN_TIME)
}

export function getFullName() {
  if (getToken()) {
    try {
      const decodeJwt: any = jwt(getToken())
      return decodeJwt?.userName?.toString() || ''
    } catch (error) {
      return 'Administrator'
    }
  }
  return ''
}

export function clearStorage() {
  localStorage.removeItem(CONSTANTS.TOKEN_STORAGE)
  localStorage.removeItem(CONSTANTS.EXPIRE_TOKEN_TIME)
  localStorage.removeItem(CONSTANTS.REFRESH_TOKEN)
}
