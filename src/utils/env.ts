export const isMock = () => {
  return import.meta.env.VITE_APP_ENV === 'MOCK'
}

export const isDev = () => {
  return import.meta.env.VITE_APP_ENV === 'DEV'
}

export const getBaseURL = () => {
  return isMock()
    ? import.meta.env.VITE_BASE_URL_MOCK
    : isDev()
    ? import.meta.env.VITE_BASE_URL_DEV
    : import.meta.env.VITE_BASE_URL
}
