import { AppBar, Toolbar, Typography } from '@mui/material'
import React from 'react'
import { ChatErrors, ChatHistory, ChatInput } from '../../chat'
import { ThemeSwitch } from '../../theme'
import { useStyles } from './element'

export const HomeComponents = () => {
  const { classes } = useStyles()
  return (
    <>
      <AppBar>
        <Toolbar className={classes.toolbar}>
          <Typography className={classes.title} noWrap>{`Vite React Template – v${import.meta.env.VITE_APP_VERSION}`}</Typography>
          <ThemeSwitch />
        </Toolbar>
      </AppBar>
      <div className={classes.main}>
        <ChatInput />
        <ChatHistory />
      </div>
      <ChatErrors />
    </>
  )
}
