import { makeStyles } from '../../utils'

export const useStyles = makeStyles()((theme) => ({
  toolbar: {
    display: 'grid',
    gridTemplateColumns: 'auto auto',
    justifyItems: 'end',
    [theme.breakpoints.down('md')]: {
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
    },
  },
  title: {
    [theme.breakpoints.down('md')]: {
      fontSize: theme.typography.caption.fontSize,
    },
  },
  main: {
    display: 'grid',
    height: '100vh',
    gridTemplateRows: 'auto 1fr',
    gridRowGap: 8,
    padding: '100px 6vw 0 6vw',
    [theme.breakpoints.down('md')]: {
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
    },
  },
}))
