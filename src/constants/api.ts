const api = {
  AUTH: {
    DEFAULT: 'auth',
    LOGIN: 'login',
    OTP: 'otp',
  },
  USER: {
    DEFAULT: 'users',
  },
}

export default api
