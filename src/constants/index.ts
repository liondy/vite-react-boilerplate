import CONSTANTS from './localStorage'
import SERVICE from './service'
import MENU from './menu'
import SLUG from './slug'
import API from './api'

export { CONSTANTS, SERVICE, MENU, API, SLUG }
