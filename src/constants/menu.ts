const menu = {
  CHAT_HISTORY: 'chat-history',
  CHAT_ERROR: 'chat-error',
}

export default menu
