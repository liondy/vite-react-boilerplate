const localStorage = {
  TOKEN_STORAGE: 'APP_TOKEN',
  EXPIRE_TOKEN_TIME: 'APP_TOKEN_EXPIRED',
  REFRESH_TOKEN: 'APP_REFRESH_TOKEN',
}

export default localStorage
