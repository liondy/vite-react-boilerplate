import { getBaseURL } from '../utils/env'

const service = {
  BASE_URL: getBaseURL() || `${window.location.protocol}//${window.location.host}/api/`,
  REQUEST_METHODS: {
    GET: 'GET',
    POST: 'POST',
    PUT: 'PUT',
    DELETE: 'DELETE',
  },
}

export default service
