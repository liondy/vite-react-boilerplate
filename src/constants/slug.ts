const slug = {
  CHAT_HISTORY: 'chatHistory',
  CHAT_ERROR: 'chatError',
}

export default slug
