export interface IResult {
  statusCode: number | null
  message: string | null
  data: any
}
