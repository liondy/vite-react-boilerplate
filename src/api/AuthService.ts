/* eslint-disable class-methods-use-this */
import { API } from '../constants'
import Service from './service'

export default class AuthService extends Service {
  constructor() {
    super(API.AUTH.DEFAULT)
  }
}
