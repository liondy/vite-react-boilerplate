/* eslint-disable class-methods-use-this */
import axios from './config'
import { IResult } from '../interfaces/IService'
import { clearStorage } from '../../utils/storage'

export default class Service {
  url: string

  constructor(url: string, baseUrl = '') {
    this.url = url
    if (baseUrl !== '') {
      axios.defaults.baseURL = baseUrl
    }
  }

  private generateBlobErrorMessage = (error: any, blobError: string): IResult => {
    const { message } = JSON.parse(blobError)
    return {
      statusCode: error.response.status,
      message,
      data: null,
    }
  }

  private generateErrorMessage = (error: any): IResult => {
    if (error?.response && error?.response.status) {
      const { status } = error.response
      if (status === 401) {
        clearStorage()
        window.location.reload()
      }
      const { message } = error.response.data
      return {
        statusCode: status,
        message,
        data: null,
      }
    }
    // if there's status is no connection
    return {
      statusCode: null,
      message: error.message || 'No Internet Connection',
      data: null,
    }
  }

  private generateDefaultResult = (result: any): IResult => ({
    statusCode: result.statusCode,
    message: result?.message,
    data: result?.data,
  })

  async findAll(
    page: number | null,
    limit: number | null,
    sort?: string,
    sortType?: string,
    params = {},
    config = {}
  ): Promise<IResult> {
    try {
      const result = await axios.get(`${this.url}`, {
        params: {
          page,
          limit,
          sort,
          sortType,
          ...params,
        },
        ...config,
      })
      return this.generateDefaultResult(result.data)
    } catch (error) {
      return this.generateErrorMessage(error)
    }
  }

  async get(path?: string, config?: any): Promise<IResult> {
    try {
      const result = await axios.get(`${this.url}${path ? `/${path}` : ''}`, config)
      return this.generateDefaultResult(result)
    } catch (error) {
      return this.generateErrorMessage(error)
    }
  }

  async create(data: any, path: string = ''): Promise<IResult> {
    const addedPath = path !== '' ? `/${path}` : ''
    try {
      const result = await axios.post(`${this.url}${addedPath}`, data)
      return this.generateDefaultResult(result)
    } catch (error) {
      return this.generateErrorMessage(error)
    }
  }

  async update(id: string | number, data: any, config?: any): Promise<IResult> {
    try {
      const result = await axios.put(`${this.url}${id ? `/${id}` : ''}`, data, config)
      return this.generateDefaultResult(result.data)
    } catch (error) {
      return this.generateErrorMessage(error)
    }
  }

  async downloadFile(config: any): Promise<IResult> {
    const newConfig = config || {
      responseType: 'blob',
    }
    try {
      const result = await axios.get(this.url, newConfig)
      return this.generateDefaultResult(result.data)
    } catch (error: any) {
      if (error.response.data instanceof Blob) {
        const blobError = await error.response.data.text()
        return this.generateBlobErrorMessage(error, blobError)
      }
      return this.generateErrorMessage(error)
    }
  }

  async uploadFile(file: File, config: any): Promise<IResult> {
    const payload = new FormData()
    payload.append('file', file)
    const newConfig = {
      ...config,
      headers: {
        ...config.headers,
        'Content-Type': 'multipart/form-data',
      },
    }
    try {
      const result = await axios.post(this.url, payload, newConfig)
      return this.generateDefaultResult(result.data)
    } catch (error) {
      return this.generateErrorMessage(error)
    }
  }
}
