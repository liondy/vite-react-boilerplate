/* eslint-disable no-param-reassign */
import axios from 'axios'
import { SERVICE } from '../../constants'
import { getRefreshToken, getToken, setRefreshToken, setToken } from '../../utils/storage'

const { CancelToken } = axios
export { CancelToken }

const ax = axios.create({
  baseURL: SERVICE.BASE_URL,
})

ax.interceptors.request.use(
  async (config: any) => {
    const token = getToken()
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }

    if (config.data instanceof FormData) {
      config.headers['content-type'] = 'multipart/form-data'
    }

    return config
  },
  (error: any) => Promise.reject(error)
)

ax.interceptors.response.use(
  (res) => res,
  async (err) => {
    const originalConfig = err.config
    if (err.response) {
      // Access Token was expired
      if (err.response.status === 401 && err.response.data?.errorCode !== 'REFRESH-TOKEN/EXPIRED') {
        try {
          const refrshToken = getRefreshToken()
          const rs = await ax.get(`UserAuth/refreshtoken/${refrshToken}`)
          const { token, refreshToken } = rs.data
          setToken(token)
          setRefreshToken(refreshToken)
          ax.defaults.headers.common.Authorization = `Bearer ${token}`
          return ax(originalConfig)
        } catch (_error: any) {
          return Promise.reject(_error)
        }
      }
    }
    return Promise.reject(err)
  }
)

export default ax
