import React from 'react'

interface ILayout {
  children: React.ReactNode
}

const Layout = (props: ILayout) => {
  const { children } = props
  return (
    <div style={{ padding: '1.5rem' }}>
      {children}
    </div>
  )
}

export default Layout
