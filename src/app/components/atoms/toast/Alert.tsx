import { forwardRef } from 'react'
import { Alert as MuiAlert, AlertProps } from '@mui/material'

const Alert = forwardRef<HTMLDivElement, AlertProps>((props, ref) => (
  <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />
))

export default Alert
