import React from 'react'
import { Snackbar } from '@mui/material'
import Alert from './Alert'

interface ToastProps {
  open: boolean
  handleClose: (event?: React.SyntheticEvent | Event) => void
  message: string
  severity?: any
}

export const Toast = ({ open, handleClose, message, severity = 'success' }: ToastProps) => {
  return (
    <Snackbar open={open} autoHideDuration={2000} onClose={handleClose}>
      <Alert onClose={handleClose} severity={severity} sx={{ width: '100%' }}>
        {message}
      </Alert>
    </Snackbar>
  )
}
