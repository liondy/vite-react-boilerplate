import { makeStyles } from '../../../../utils'

export const useStyles = makeStyles()((theme) => ({
  avatar: {
    backgroundColor: '#BDBDBD !important',
    fontSize: '16px',
  },
}))
