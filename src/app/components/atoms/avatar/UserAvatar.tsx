import React from 'react'
import { Avatar } from '@mui/material'
import { useStyles } from './element'
import _ from 'lodash'

interface IUserAvatar {
  name: string
}

export const UserAvatar = (props: IUserAvatar) => {
  const { name } = props

  const { classes } = useStyles()

  const AvatarName = (name: string) => {
    const initials = _.split(name, ' ')
      .map((initial) => initial[0])
      .join('')
    return initials
  }

  return <Avatar className={classes.avatar}>{AvatarName(name)}</Avatar>
}
