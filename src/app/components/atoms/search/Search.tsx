import React from 'react'
import SearchIcon from '@mui/icons-material/Search'

import { Search as SearchContainer, SearchIconWrapper, StyledInputBase } from './element'

export const Search = () => {
  return (
    <SearchContainer>
      <SearchIconWrapper>
        <SearchIcon />
      </SearchIconWrapper>
      <StyledInputBase placeholder="Search…" inputProps={{ 'aria-label': 'search' }} />
    </SearchContainer>
  )
}
