import React from 'react'
import { Routes, Route, Navigate } from 'react-router-dom'
import routes from '../../routes'
import PrivateRoute from '../../routes/PrivateRoutes'
import { Loading } from './atoms'
import Layout from './layouts/Layout'
import { Login } from '../../auth'

export const App = () => {
  return (
    <Routes>
      <Route
        path="/login"
        element={
          <React.Suspense fallback={<Loading />}>
            <Login />
          </React.Suspense>
        }
      />
      {routes.map(({ key, path, component: Component, children, navigateTo }) =>
        Component ? (
          <Route
            key={key}
            path={path}
            element={
              <React.Suspense fallback={<Loading />}>
                <PrivateRoute>
                  <Layout>
                    <Component />
                  </Layout>
                </PrivateRoute>
              </React.Suspense>
            }
          />
        ) : (
          <React.Fragment key={key}>
            {children ? (
              <>
                {children.map(
                  ({ key: childKey, path: childPath, component: ChildComponent, navigateTo: childNavigateTo }) => (
                    <React.Fragment key={childKey}>
                      {!childNavigateTo && (
                        <Route
                          key={childKey}
                          path={childPath}
                          element={
                            <React.Suspense fallback={<Loading />}>
                              <PrivateRoute>
                                <Layout>
                                  <ChildComponent />
                                </Layout>
                              </PrivateRoute>
                            </React.Suspense>
                          }
                        />
                      )}
                      {!!childNavigateTo && (
                        <Route key={childKey} path={childPath} element={<Navigate to={childNavigateTo} />} />
                      )}
                    </React.Fragment>
                  )
                )}
                {!!navigateTo && <Route key={key} path={path} element={<Navigate to={navigateTo} />} />}
              </>
            ) : (
              <></>
            )}
          </React.Fragment>
        )
      )}
    </Routes>
  )
}
