/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_APP_VERSION: string
  readonly VITE_APP_ENV: string
  readonly VITE_BASE_URL: string
  readonly VITE_BASE_URL_MOCK: string
  readonly VITE_BASE_URL_DEV: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
