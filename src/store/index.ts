import { combineReducers, configureStore } from '@reduxjs/toolkit'
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux'
import { themeReducer } from '../theme'
import { chatReducer } from '../chat'
import { authReducer } from '../auth/slices/authSlice'

export const reducer = combineReducers({
  theme: themeReducer,
  chat: chatReducer,
  auth: authReducer,
})

export type RootState = ReturnType<typeof reducer>

export const createStore = () =>
  configureStore({
    reducer,
  })

export const store = createStore()

export type AppDispatch = typeof store.dispatch

export const useAppDispatch: () => AppDispatch = useDispatch
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector
