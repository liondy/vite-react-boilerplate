import { Navigate, RouteProps } from 'react-router-dom'
import { getToken } from '../utils/storage'

interface PrivateRouteProps extends RouteProps {
  children?: any
}

const PrivateRoute = (props: PrivateRouteProps) => {
  const { children } = props
  console.log('token?? ', getToken())
  return getToken() !== '' ? children : <Navigate to="/login" />
}

export default PrivateRoute
