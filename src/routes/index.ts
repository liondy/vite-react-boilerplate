import IRoutes from './interfaces/routes'
import chatRoutes from './chat/chat'
import themeRoutes from './theme/theme'
import homeRoutes from './home/home'

const routes: IRoutes[] = [...homeRoutes, ...chatRoutes, ...themeRoutes]

export default routes
