import React from 'react'
import MailIcon from '@mui/icons-material/Mail'

const ThemeIcon = (key: string) => {
  switch (key) {
    default:
      return <MailIcon />
  }
}

export default ThemeIcon
