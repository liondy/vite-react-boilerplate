import React from 'react'
import MailIcon from '@mui/icons-material/Mail'

const ChatIcon = (key: string) => {
  switch (key) {
    default:
      return <MailIcon />
  }
}

export default ChatIcon
