import { ThemeSwitch } from '../../theme'
import ThemeIcon from '../icons/ThemeIcon'
import IRoutes from '../interfaces/routes'

const themeRoutes: IRoutes[] = [
  {
    key: 'theme',
    path: '/theme',
    type: 'sidebar',
    name: 'Theme',
    component: ThemeSwitch,
    icon: ThemeIcon('chat'),
  },
]

export default themeRoutes
