export default interface IRoutes {
  key: string
  path: string
  component?: any
  type: string
  name: string
  children?: Array<any>
  childrenPath?: Array<string>
  icon: any
  navigateTo?: string
}
