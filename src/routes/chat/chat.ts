import IRoutes from '../interfaces/routes'
import { ChatHistory, ChatErrors } from '../../chat'
import { MENU, SLUG } from '../../constants'
import ChatIcon from '../icons/ChatIcon'

const chatRoutes: IRoutes[] = [
  {
    key: 'chat',
    path: 'chat',
    type: 'sidebar',
    name: 'Chat',
    children: [
      {
        key: SLUG.CHAT_HISTORY,
        path: MENU.CHAT_HISTORY,
        component: ChatHistory,
        type: '',
        name: 'Chat History',
      },
      {
        key: SLUG.CHAT_ERROR,
        path: MENU.CHAT_ERROR,
        component: ChatErrors,
        type: '',
        name: 'Chat Error',
      },
    ],
    icon: ChatIcon('chat'),
  },
]

export default chatRoutes
