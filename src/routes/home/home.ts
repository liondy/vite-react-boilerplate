import { Home } from '../../home'
import ThemeIcon from '../icons/ThemeIcon'
import IRoutes from '../interfaces/routes'

const homeRoutes: IRoutes[] = [
  {
    key: 'home',
    path: '/',
    type: '',
    name: 'Home',
    component: Home,
    icon: ThemeIcon('chat'),
  },
]

export default homeRoutes
